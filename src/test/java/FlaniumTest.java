import FlaNium.WinAPI.webdriver.DesktopOptions;
import FlaNium.WinAPI.webdriver.FlaNiumDriver;
import FlaNium.WinAPI.webdriver.FlaNiumDriverService;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;

public class FlaniumTest {
    static DesktopOptions options;
    static FlaNiumDriverService service;
    static FlaNiumDriver driver;

    @BeforeTest
    public static void setupEnvironment(){

        String DRIVER_PATH = "C:\\FlaNium\\FlaNium.Driver.exe";
        String APP_PATH = "C:\\Windows\\System32\\notepad.exe";
        int driverPort = 9999;

        // Инициализация драйвера:
        service = new FlaNiumDriverService.Builder()
                // Указание пути до драйвера
                .usingDriverExecutable(new File(DRIVER_PATH).getAbsoluteFile())
                // Установка порта (по умолчанию 9999)
                .usingPort(driverPort)
                // Включение режима отладки (вывод логов в консоль)
                .withVerbose(true)
                // Отключение логирования
                .withSilent(false)
                .buildDesktopService();

        // Инициализация приложения:
        options = new DesktopOptions();
        // Указание пути до тестируемого приложения
        options.setApplicationPath(String.valueOf(new File(APP_PATH)));
        // Задержка после запуска приложения (сек)
        options.setLaunchDelay(5);
        // Подключение к ранее запущенному экземпляру приложения
        options.setDebugConnectToRunningApp(false);

        // Получение экземпляра драйвера приложения
        driver = new FlaNiumDriver(service, options);
    }

    @Test
    public void calculatorTest() throws InterruptedException {
        Thread.sleep(3000);

        driver.findElementByName("Справка").click();
        driver.findElementByName("О программе").click();
        driver.findElementByName("ОК").click();
    }

    @AfterTest
    public void tearDown(){
        service.stop();
    }
}
